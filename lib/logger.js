var winston = require('winston');

winston.cli();

var DEBUG_LEVEL = process.env.DEBUG_LEVEL || 'info'


var logger = new winston.Logger({
  transports: [
    new winston.transports.Console({level: DEBUG_LEVEL, timestamp: true})
  ]
});

logger.cli();

exports.logger = logger;
